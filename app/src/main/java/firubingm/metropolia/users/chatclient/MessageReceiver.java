package firubingm.metropolia.users.chatclient;

import android.os.Handler;
import android.os.Message;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;


public class MessageReceiver extends Thread {

    private Handler handler;
    private Socket socket;

    public MessageReceiver(Handler handler, Socket socket) {
        this.handler = handler;
        this.socket = socket;
    }

    public void run() {
        try {
            InetSocketAddress address = new InetSocketAddress("10.0.2.2", 8777);
            this.socket.connect(address);
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            while (true) {
                String messageContent = reader.readLine();
                if (messageContent == null) {
                    break;
                }
                Message message = this.handler.obtainMessage();
                message.what = 0;
                message.obj = messageContent + "\n";
                handler.sendMessage(message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
