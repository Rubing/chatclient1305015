package firubingm.metropolia.users.chatclient;


import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class MessageSender extends Thread {

    Socket socket;
    String message;

    public MessageSender(Socket socket, String message) {
        this.socket = socket;
        this.message = message;
    }

    public void run() {
        if (!socket.isConnected()) {
            return;
        }
        try {
            PrintWriter writer = new PrintWriter(this.socket.getOutputStream(), true);
            writer.println(this.message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
