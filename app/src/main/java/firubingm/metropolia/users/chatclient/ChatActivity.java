package firubingm.metropolia.users.chatclient;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import java.net.Socket;

public class ChatActivity extends Activity implements View.OnClickListener {

    private Handler uiHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 0) {
                TextView result = (TextView)findViewById(R.id.textView);
                result.append((String) msg.obj);
                ScrollView scrollView = (ScrollView)findViewById(R.id.scrollView);
                scrollView.smoothScrollTo(0, scrollView.getBottom());
            }
        }
    };
    Socket socket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        // Button
        Button button = (Button)findViewById(R.id.button);
        button.setOnClickListener(this);

        // Socket
        this.socket = new Socket();

        // Message Receiver
        MessageReceiver receiver = new MessageReceiver(uiHandler, this.socket);
        receiver.start();
    }


    public void onClick(View v) {
        EditText textBar = (EditText) findViewById(R.id.textBar);
        String message = textBar.getText().toString();
        if (message.isEmpty()) {
            return;
        }
        MessageSender sender = new MessageSender(this.socket, message);
        sender.start();
        textBar.setText("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
